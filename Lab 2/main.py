
"""
@file       main.py
@brief      Runs tasks that creates Encoder Interface
@details    Runs task_encoder.py and task_user.py to set up the user interface and update encoder position.
@author     Cesar Santana
@author     Dylan Ruiz
@date       10/21/2021
"""

import task_user
import task_encoder
import encoder
import shares

def main():
    ''' @brief      The main program runs both Tasks
        @details    Main runs both task_encoder and task_user in a while loop
    '''
    

    ## A shares.Share object used by the tasks for sharing the encoder position    
    pos_share = shares.Share(0)
    
    ## A shares.Share object used by the tasks for sharing the change in encoder position
    delta_share = shares.Share(0)
    
    
  
    ## A shares.Share object used by the tasks for sharing the change in a flag variable used to run different encoder functions
    enc_flag = shares.Share(0)
    
    ## initializes encoder.py
    encoder.Encoder()
    
    ## A task.encoder.Task_Encoder object used to run the encoder task continuously  
    task1 = task_encoder.Task_Encoder(5_000, pos_share, delta_share, enc_flag)
    
    ##  A task.encoder.Task_Encoder object used to run the encoder task continuously 
    task2 = task_user.Task_User(5_000, pos_share, delta_share, enc_flag)
    
    ## task list used to run in while loop
    task_list = [task1, task2]
    
    while(True):
        try: 
            for task in task_list:
                task.run()
        except KeyboardInterrupt:
            break
            print('Program Terminating')
    
if __name__ =='__main__':
   main()

