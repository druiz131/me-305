'''
    @file       task_encoder.py
    @brief      Updates encoder object at regualr intervals
    @details    Implements a finite state machine that performs different actions 
                based on flag indication.
'''

import time
import encoder


class Task_Encoder:
    ''' @brief      Encoder task for encoder
        @details    Implements a finite state machine
    '''
    
    def __init__(self, period, pos_share, delta_share, enc_flag):
        ''' @brief              Constructs an Encoder task.
            @details            The Encoder task continuously updates the encoder
                                position at a given period and runs encoder functions
                                when prompted by a flag.
            @param period       The period, in microseconds, between runs of the task.
            @param pos_share    A shares.Share object used to hold the position of the encoder. 
            @param delta_share  A shares.Share object used to hold the change of position of the encoder.
            @param enc_flag     A shares.Share object used to indicate which encoder function to run.
        '''
        ## A shares.Share object representing the position of the encoder
        self.pos_share = pos_share
        ## A shares.Share object representing the change of position of the encoder
        self.delta_share = delta_share
        ## A shares.Share object representing the indicator for which encoder function to run
        self.enc_flag = enc_flag
        ## The number of runs of the state machine     
        self.runs = 0
        ## The period (in us) of the task
        self.period = period
        ## The time to run the next iteration of the task
        self.next_time = time.ticks_add(time.ticks_us(), self.period) 
        ## A encoder.Encoder object used to run encoder function in the task
        self.encode = encoder.Encoder()
        
    def run(self):
        ''' @brief      Runs one iteration of the FSM.
            @details    Continuously updates encoder position while running 
                        other encoder function when prompted to through the enc_flag.
        '''
        if(time.ticks_us() >= self.next_time):
                if self.enc_flag.read() == 1:
                    self.pos_share.write(self.encode.set_position(0))
                    
                elif self.enc_flag.read() == 2:
                    self.delta_share.write(self.encode.get_delta())
                    
                else:
                 
                    self.encode.update()
                    self.pos_share.write(self.encode.get_position())
                   
                    self.delta_share.write(self.encode.get_delta())
                 
                    
        
         
                    self.next_time += self.period
                    self.runs += 1
