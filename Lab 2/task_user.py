'''
    @file       task_user.py
    @brief      User interface task for cooperative multitasking of encoder 
    @details    Implements a finite state machine that performs different actions 
                based on character input.
'''

import time
import pyb

## State 0 of the user interface task
s0_init = 0
## State 1 of the user interface task
s1 = 1
## State 2 of the user interface task
s2 = 2
## State 3 of the user interface task
s3 = 3
class Task_User:
    '''@brief       User interface task for encoder project.
       @details     Implements a finite state machine.
    '''

    def __init__(self, period, pos_share, delta_share, enc_flag):
        ''' @brief              Contructs a user interface task.
            @details            The user interface task is implemented as a finite state machine.
            @param period       The period, in microseconds, between runs of the task.
            @param pos_share    A shares.Share object used to hold the position of the encoder. 
            @param delta_share  A shares.Share object used to hold the change of position of the encoder.
            @param enc_flag     A shares.Share object used to indicate which encoder function to run.
        '''
        ## A shares.Share object representing the position of the encoder
        self.pos_share = pos_share
        ## A shares.Share object representing the change of position of the encoder
        self.delta_share = delta_share
        ## A shares.Share object representing the indicator for which encoder function to run
        self.enc_flag = enc_flag

        ## The state to run on the next iteration of the finite state machine
        self.state = s0_init
        ## The period (in us) of the task 
        self.period = period
        ## The number of runs of the state machine
        self.runs = 0
        ## A serial port to use for user I/O
        self.ser = pyb.USB_VCP()
        ## The time to run the next iteration of the task
        self.next_time = time.ticks_add(time.ticks_us(), self.period)
        ## Starts the enc flag at state 0
        self.enc_flag.write(0)
        ## Variable used to index through lists
        self.idx = 0
        ## List used to display encoder position while collecting data
        self.my_list_pos = []
        ## List used to display time while collecting data
        self.my_list_time = []
        ## The period used to collect data during state 2
        self.t_period = 100
        ## The time to run the next iteration state 2
        self.next_time2= time.ticks_add(time.ticks_ms(),self.t_period)
        ## The reference time when entering state 2
        self.Tref = 0
        
    def run(self):
        ''' @brief      Runs one iteration of the FSM
        '''
        
        self.instruct = """
        ------------------------------------------------------------------------
        Use the following inputs to perform their correlated actions           |
        ------------------------------------------------------------------------
        |z|Zero the position of encoder 1                                      |
        |p|Print out the position of encoder 1                                 |
        |d|Print out the delta for encoder 1                                   | 
        |g|Collect encoder 1 data for 30 sec and print [position,time] list    |
        |s|End Data Collection prematurely                                     |
        ------------------------------------------------------------------------                                                                    
        """
        current_time =  time.ticks_us()  
        if (time.ticks_diff(current_time, self.next_time) >= 0):
          
            if self.state == s0_init:
                print(self.instruct) 
                self.state = s1
            elif self.state == s1:
                self.enc_flag.write(0)
                if self.ser.any():
                    
                    self.char_in = self.ser.read(1).decode()
                    if self.char_in == 'z' or self.char_in == 'Z':
                        self.enc_flag.write(1)
                      
                       
                        print('Position Zeroed')
                      
                        
                    elif self.char_in == 'p' or self.char_in == 'P':
                       
                        print('Encoder Position is {:}'.format(self.pos_share.read()))
                        
                        
                    elif self.char_in == 'd'or self.char_in == 'D':
                        self.enc_flag.write(2)

                        print('Delta is {:}'.format(self.delta_share.read()))
                        
                    elif self.char_in == 'g' or self.char_in == 'G':
                        self.state = s2
                        self.Tref = time.ticks_ms()
                        self.end_30 = time.ticks_add(self.Tref, 30000)
                        print('Collecting Data on encoder 1....')
                        
                    elif self.char_in == 'h' or self.char_in == 'H':
                        print(self.instruct) 
                    else:
                        print('Command \'{:}\' is invalid.'.format(self.char_in))
                        pass
                        
            elif self.state == s2:
                        
                        self.enc_flag.write(0)
                        self.idx = 0
                        if self.ser.any():
                            self.char_in = self.ser.read(1).decode()
                            if self.char_in == 's' or self.char_in == 'S':
                                self.state = s3
                        if (time.ticks_ms() >= self.end_30):
                            self.state = s3
                        if(time.ticks_ms() >= self.next_time2):
                        
                            self.pos_share.read()
                            self.my_list_pos.append(self.pos_share.read())
                            self.my_list_time.append(time.ticks_diff(time.ticks_ms(),self.Tref))
                            self.next_time2= time.ticks_add(time.ticks_ms(),self.t_period)
                            
                        
            
                     
            elif self.state == s3:
                for idx in range(len(self.my_list_pos)):
                            print('[{:},{:}]'.format(self.my_list_pos[idx],
                                  self.my_list_time[idx]/1000))
                            
                self.state = s1
                self.my_list_pos = []
                self.my_list_time = []            
            else:
                raise ValueError('Invalid State')
                
            self.next_time = time.ticks_add(self.next_time, self.period)
            self.runs += 1

