"""
    @file           encoder.py
    @brief          Driver class for encoder project.
    @details        Defines functions used in the task_encoder. 
"""

import pyb

class Encoder:
    ''' @brief                  Interface with quadrature encoders.
        @details                Defines functions used to interface with the encoder.
    '''
    def __init__(self):
        ''' @brief              Constructs an Encoder object
            @details                     
        '''
        ## A shares.Share object representing the position of the encoder
        self.pos = 0
        
        ## A shares.Share object represent in the change in position
        #  between the two most recent updates of the encoder
        self.delta = 0
        self.postim = 0
        
        self.pinB6 = pyb.Pin(pyb.Pin.cpu.B6)
        self.pinB7 = pyb.Pin(pyb.Pin.cpu.B7)
        ## The timer channel used for encoder 1
        self.tim4 = pyb.Timer(4, prescaler = 0, period = 65535) 
        self.t4ch1 = self.tim4.channel(1, pyb.Timer.ENC_A, pin=self.pinB6)
        self.t4ch2 = self.tim4.channel(2, pyb.Timer.ENC_B, pin=self.pinB7)
        self.pos1 = 0
        self.start = 0
        self.stop = 0
        
    def update(self):
        ''' @brief              Updates encoder position and delta
            @details
        '''
        self.stop = abs(self.tim4.counter())
        self.delta = self.stop - self.start
        
        if self.delta < -32768:
            
            self.delta = self.delta + 65536
            
        elif self.delta > 32768:
            self.delta = self.delta - 65536
            
        self.pos += self.delta
        #self.pos1 = self.pos
        self.start = self.stop
        
    def get_position(self):
        ''' @brief              Returns encoder position
            @details
            @return             The position of the encoder shaft
        '''
        return self.pos
    
    def set_position(self, position):
        ''' @brief              Sets encoder position
            @details
            @param position     Resets the position to the desired
                                user input
        '''
        
        self.start = abs(self.tim4.counter())
        self.pos = position
        
        
    def get_delta(self):
        ''' @brief              Returns encoder delta
            @details
            @return             The change in position of the encoder shaft
                                between the two most recent updates
        '''
        
        return self.delta