"""
    @file           TP_imu.py
    @brief          Driver class that sets up and receives data from an IMU.
"""

from pyb import I2C
import struct
import time
import os

class TP_IMU:
    ''' @brief      An IMU driver class 
        @details    Objects of this class can be used to recieve IMU data.
    '''
    ## Constructor
    def __init__(self):
        ''' @brief      Initializes and returns an IMU object 
            @details    Initializes the IMU in master mode, allowing the 
                        retrieval of data.
        '''
        self.i2c = I2C(1, I2C.MASTER)
        
    ## Method to change operaring mode of IMU
    def op_mode(self, register):
        ''' @brief          Method to change operating mode of IMU.
            @param register Register address of operating mode of IMU.
        '''
        self.i2c.mem_write(register, 0x28, 0x3D)
        
    ## Method to retrieve calibration status from IMU
    def cal_status(self):
        ''' @brief     Method to retrieve calibration status from IMU 
            
        '''
        return self.i2c.mem_read(1, 0x28, 0x35)
        
    ## Method to retrieve calibration coefficients from IMU
    def cal_coeff(self):
        ''' @brief   Method to retrieve calibration coefficients from IMU
            @details
        '''
        return self.i2c.mem_read(22, 0x28, 0x55)
    
    ## Method to write calibration coefficients to the IMU from pre-recorded data
    def set_cal(self, coeff):
        ''' @brief        Method to write calibration coefficients to the IMU from pre-recorded data
            @param coeff  Calibration coefficients to write to IMU.
        '''
        self.i2c.mem_write(coeff, 0x28, 0x1A)
        
    ## Method to read Euler angles from the IMU to use as state measurements
    def eul(self):
        ''' @brief Method to read Euler angles from the IMU to use as state measurements
            
        '''
        self.eul_signed_ints = struct.unpack('<hhh', self.i2c.mem_read(6, 0x28, 0x1A))
        self.eul_vals = tuple(self.eul_int/16 for self.eul_int in self.eul_signed_ints)
        return self.eul_vals
    
    ## Method to read angular velocity from the IMU to use as  state measurements
    def omega(self):
        ''' @brief Method to read angular velocity from the IMU to use as  state measurements
        '''
        self.omega_signed_ints = struct.unpack('<hhh', self.i2c.mem_read(6, 0x28, 0x14))
        self.omega_vals = tuple(self.omega_int/16 for self.omega_int in self.omega_signed_ints)
        return self.omega_vals
    
#if __name__ == '__main__':
#     IMU = TP_IMU()
#     config = 0b0000
#     NDOF = 0b1100
#     if 'CalibrationCoefficients' in os.listdir():
#         print('Import Calibration Coefficients...')
#         IMU.op_mode(config)
#         with open('CalibrationCoefficients' , 'r') as f:
#             IMU.set_cal(f.read())
#         IMU.op_mode(NDOF)
#     else:
#         IMU.op_mode(NDOF)
#         calstat = IMU.cal_status()[0]
#         print('calibrating')
#         mag_calib = False
#         acc_calib = False
#         gyr_calib = False
#         while calstat != 0b11111111:
#             if ((calstat & 0b11) == 0b11) & (mag_calib == False):
#                 print('magnetometer calibrated')
#                 mag_calib = True
#             if ((calstat & 0b1100) == 0b1100) & (acc_calib == False):
#                 print('accelerometer calibrated')
#                 acc_calib = True
#             if ((calstat & 0b110000) == 0b110000) & (gyr_calib == False):
#                 print('gyroscope calibrated')
#                 gyr_calib = True
#             calstat = IMU.cal_status()[0]
#         print('calibrated')
#        
#         cal = IMU.cal_coeff()
#         print(cal)
#         cal_list = open('CalibrationCoefficients', 'w')
#         cal_list.write(cal)
#         cal_list.close()
        
#     print('(Heading, Roll, Pitch) deg: ')
#    
#     while True:
#         angles = IMU.eul()
#         print(angles)
#         ang_vel = IMU.omega()
#         print(ang_vel)
#         time.sleep(1)