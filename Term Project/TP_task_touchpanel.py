"""
    @file           TP_task_touchpanel.py
    @brief          Task that receives and shares data from the touchpanel.   
"""
import TP_touchpanel
import utime
import pyb

class TP_Task_TouchPanel:
    ''' @brief          TouchPanel Task Class
        @details        This task receives and shares data from the touchpanel,
                        while also calculating the velocities of the ball.
    '''
    
    def __init__(self, period, x_share, y_share, z_share, Vx_share, Vy_share):
        ''' @brief              Constructs a touch panel task.
            @details            The touch panel task continuously updates the position of
                                the ball using x and y coordintes, as well as checking to 
                                see if there is anything incontact with the plate.
            @param period       The period, in microseconds, between runs of the task.
            @param x_share      A shares.Share object reepresenting the x posiion of the ball
            @param y_share      A shares.Share object representing the y position of the ball
            @param z_share      A shares.Share object representing whether or not something is in contact with the plate
            @param Vx_share     A shares.Share object representing the x velocity of the ball
            @param Vy_share     A shares.Share object representing the y velocity of the ball
        '''
        
        ## The period (in us) of the task
        self.period = period
        
        ## A shares.Share object that represents the x position of the ball
        self.x_share = x_share
        
        ## A shares.Share object that represents the y position of the ball
        self.y_share = y_share
        
        ## A shares.Share object that represent whether or not the ball is touching the plate
        self.z_share = z_share
        
        ## A shares.share object that represents the x velocity of the ball
        self.Vx_share = Vx_share
        
        ## A shares.Share object that represents the y velocity of the ball
        self.Vy_share = Vy_share
        
        ## A TP_touchpanel.TP_TouchPanel object that is used to run the scaning functions
        self.TouchPanel = TP_touchpanel.TP_TouchPanel(pyb.Pin.cpu.A7, pyb.Pin.cpu.A6, pyb.Pin.cpu.A1, pyb.Pin.cpu.A0, 176, 100, 86, 45)
        
        ## The time to run the next iteration of the task
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        
        ## The initial time used to calculate the velocity of the ball in the x direction
        self.start_x = utime.ticks_us()
        
        ## The initial time used to calculate the velocity of the ball in the y direction
        self.start_y = utime.ticks_us()
        
        ## The number of runs of the state machine
        self.runs = 0
        
        ## The initial x position of the ball
        self.x1 = self.TouchPanel.scan_xyz(1)
        
        ## The initial y position of the ball
        self.y1 = self.TouchPanel.scan_xyz(2)
        
    def run(self):
        ''' @brief      Runs one itration of FSM
            @details    Continuously updates the ball's position and velocity on the touch
                        panel and checks to see if ball is making contact with panel
        '''
        if(utime.ticks_us() >= self.next_time):
            
            self.z = self.TouchPanel.scan_xyz(3)
            self.z_share.write(self.z)
            
            self.x2 = self.TouchPanel.scan_xyz(1)
            self.y2 = self.TouchPanel.scan_xyz(2)
            
            self.stop_x = utime.ticks_us()
            self.stop_y = utime.ticks_us()
            
            
            self.delta_x = self.x2 - self.x1
            self.delta_y = self.y2 - self.y1
            
            self.tim_x = (self.stop_x - self.start_x)/1000000
            self.tim_y = (self.stop_y - self.start_y)/1000000
            
            self.x_share.write(self.x2)
            self.y_share.write(self.y2)
            
            self.Vx_share.write(self.delta_x/self.tim_x)
            self.Vy_share.write(self.delta_y/self.tim_y)
            
            self.start_x = self.stop_x
            self.start_y = self.stop_y
            
            self.x1 = self.x2
            self.y1 = self.y2
            
        else:
            self.next_time += self.period
            self.runs += 1
            


            
 
                