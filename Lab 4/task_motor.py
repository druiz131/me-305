'''
    @file       task_motor.py
    @brief      Sets the duty cycle for motors
    @details    Implements a finite state machine that performs different actions 
                based on flag indication.
'''


import time
import motor
import closedloop

class Task_Motor:
    ''' @brief      Motor task for encoder
        @details    Impliments a finite state machine
    '''
    
    def __init__(self, period, duty, omega_share, omegameas1_share, omegameas2_share, Pgain_share, mot_flag):
        ''' @brief          Constructs a Motor task
            @details        The Motor task sets the duty cycle for a motor at
                            a given period and runs motor functions when 
                            prompted by a flag
            @param period   The period, in microseconds, between runs of the task.
            @param duty     A shares.Share object used to indicate the desired duty cycle
            @param mot_flag A shares.Share object used to indicate which motor function to run
        '''
        ## A shares.Share object representing the indicaticator for which motor function to run
        self.mot_flag = mot_flag
        ## The period (in us) of the task
        self.period = period
        
      
        self.omegameas1_share = omegameas1_share
        
        self.omegameas2_share = omegameas2_share
        
        ## A shares.Share object representing the angular speed of the encoder
        self.omega_share = omega_share
        
        ## A shares.share object representing the desired duty cycle for the motors
        self.duty_share = duty
        
        ## A shares.Share object representing the proportional gain of the controller
        self.Pgain_share = Pgain_share
        
        ## The time to run the next iteration of the task
        self.next_time = time.ticks_add(time.ticks_us(), self.period) 
        ## The number of runs of the state machine
        self.runs = 0
        
        ## A motor.DRB8847 object used to run motor functions in the task
        self.motor_drv = motor.DRV8847()
        ## Indicates motor 1
        self.motor_1 = self.motor_drv.motor(1)
        ## Indicates motor 2
        self.motor_2 = self.motor_drv.motor(2)
        
        ## A closedloop.Closed_Loop object used to run the closedloop functions
        self.closed_loop = closedloop.Closed_Loop(1, 100, -100)
        
    def run(self,):
        ''' @brief      Runs one iteration of the FSM
            @details    Sets the duty cycle for a desired motor based on a 
                        user input, when prompted by a flag
        '''
        
        if(time.ticks_us() >= self.next_time):
                if self.mot_flag.read() == 1:
                    self.motor_drv.enable()
                    self.motor_1.set_duty(self.duty_share.read())
                    self.mot_flag.write(0)
                
                elif self.mot_flag.read() == 2:
                    self.motor_drv.enable()
                    self.motor_2.set_duty(self.duty_share.read())
                    self.mot_flag.write(0)
                
                elif self.mot_flag.read() == 3:
                    self.motor_drv.enable()
                
                elif self.mot_flag.read() == 4:
#                    self.motor_drv.enable()
                    self.duty_1 = self.closed_loop.update(self.omega_share.read(), self.omegameas1_share.read())
                    self.motor_1.set_duty(self.duty_1)
                    self.duty_share.write(self.duty_1)
                    
                elif self.mot_flag.read() == 5:
#                    self.motor_drv.enable()
                    self.duty_2 = self.closed_loop.update(self.omega_share.read(), self.omegameas2_share.read())
                    self.motor_2.set_duty(self.duty_2)
                    self.duty_share.write(self.duty_2)
                
                elif self.mot_flag.read() == 6:
                    self.closed_loop.set_Kp(self.Pgain_share.read())
                    
            
                else:
         
                    self.next_time += self.period
                    self.runs += 1

