"""
    @file           encoder.py
    @brief          Driver class for encoder project.
    @details        Defines functions used in the task_encoder. 
"""

import pyb

class Encoder:
    ''' @brief                  Interface with quadrature encoders.
        @details                Defines functions used to interface with the encoder.
    '''
    def __init__(self):
        ''' @brief              Constructs an Encoder object
            @details            Initializes pin and timers, configures channels
                                and sets the value of parameters to be used
                                in encoder functions
        '''
        ## The positions of encoder 1
        self.pos_1 = 0
        ## The position of encoder 2
        self.pos_2 = 0
        
        ## The change in position for encoder 1 
        self.delta_1 = 0
        ## The change in position for encoder 2
        self.delta_2 = 0
        
        ## Initializez the B6 pin
        self.pinB6 = pyb.Pin(pyb.Pin.cpu.B6)
        ## Initializes the B7 Pin
        self.pinB7 = pyb.Pin(pyb.Pin.cpu.B7)
        ## Initializes the C6 pin
        self.pinC6 = pyb.Pin(pyb.Pin.cpu.C6)
        ##Initializes the C7 pin
        self.pinC7 = pyb.Pin(pyb.Pin.cpu.C7)
        
        ## Initializes timer 4
        self.tim4 = pyb.Timer(4, prescaler = 0, period = 65535) 
        ## Initializes timer 8
        self.tim8 = pyb.Timer(8, prescaler = 0, period = 65535)
        
        ## Configures channel 1 for timer 4
        self.t4ch1 = self.tim4.channel(1, pyb.Timer.ENC_A, pin=self.pinB6)
        ## Configures channel 2 for timer 4
        self.t4ch2 = self.tim4.channel(2, pyb.Timer.ENC_B, pin=self.pinB7)
        ## Configures channel 1 for timer 8
        self.t8ch1 = self.tim8.channel(1, pyb.Timer.ENC_A, pin=self.pinC6)
        ## Configures channel 2 for timer 8
        self.t8ch2 = self.tim8.channel(2, pyb.Timer.ENC_B, pin=self.pinC7)
        
        ## Last position of encoder 1 when calculating delta
        self.start_1 = 0
        ## Last position of encoder 2 when calculating delta
        self.start_2 = 0
        
        ## Position of encoder 1 when calculating delta
        self.stop_1 = 0
        ## Position of encoder 2 when calculating delta
        self.stop_2 = 0
        
    def update(self):
        ''' @brief              Updates encoder position and delta
            @details            Updated the encoder position and delta, as well
                                as accounting for overflow
        '''
        self.stop_1 = abs(self.tim4.counter())
        self.stop_2 = abs(self.tim8.counter())
        
        self.delta_1 = self.stop_1 - self.start_1
        self.delta_2 = self.stop_2 - self.start_2
        
        if self.delta_1 < -32768:
            
            self.delta_1 = self.delta_1 + 65536
            
        elif self.delta_1 > 32768:
            self.delta_1 = self.delta_1 - 65536
            
        self.pos_1 += self.delta_1
        self.start_1 = self.stop_1
        
        if self.delta_2 < -32768:
            
            self.delta_2 = self.delta_2 + 65536
            
        elif self.delta_2 > 32768:
            self.delta_2 = self.delta_2 - 65536
            
        self.pos_2 += self.delta_2
        self.start_2 = self.stop_2
        
    def get_position(self, encoder):
        ''' @brief              Returns encoder position
            @details            Returns the position of the encoder based on
                                the results of the update() fcn
            @return             The position of the encoder shaft
        '''
        if encoder == 1:
            return self.pos_1
        
        elif encoder == 2:
            return self.pos_2
    
    def set_position(self, position, encoder):
        ''' @brief              Sets encoder position
            @details            Sets the encoder position to the desired
                                user input
            @param position     Resets the position to the desired
                                user input
        '''
        if encoder == 1:
            self.start_1 = abs(self.tim4.counter())
            self.pos_1 = position
        elif encoder == 2:
            self.start_2 = abs(self.tim8.counter())
            self.pos_2 = position
            
        
        
    def get_delta(self, encoder):
        ''' @brief              Returns encoder delta
            @details            Returns the change in position of the encoder
                                based on the results from the update() fcn
            @return             The change in position of the encoder shaft
                                between the two most recent updates
        '''
        if encoder == 1:
            return self.delta_1
        elif encoder == 2:
            return self.delta_2