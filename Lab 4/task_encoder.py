'''
    @file       task_encoder.py
    @brief      Updates encoder object at regualr intervals
    @details    Implements a finite state machine that performs different actions 
                based on flag indication.
'''

import time
import encoder
import math

class Task_Encoder:
    ''' @brief      Encoder task for encoder
        @details    Implements a finite state machine
    '''
    
    def __init__(self, period, pos1_share, pos2_share, delta1_share, delta2_share, omegameas1_share, omegameas2_share, enc_flag):
        ''' @brief              Constructs an Encoder task.
            @details            The Encoder task continuously updates the encoder
                                position at a given period and runs encoder functions
                                when prompted by a flag.
            @param period       The period, in microseconds, between runs of the task.
            @param pos_share    A shares.Share object used to hold the position of the encoder. 
            @param delta_share  A shares.Share object used to hold the change of position of the encoder.
            @param enc_flag     A shares.Share object used to indicate which encoder function to run.
        '''
        ## A shares.Share object representing the position of encoder 1
        self.pos1_share = pos1_share
        ## A shares.Share object representing the position of encoder 2
        self.pos2_share = pos2_share
        
        ## A shares.Share object representing the change of position of encoder 1
        self.delta1_share = delta1_share
        ## A shares.Share object representing the change in position of encoder 2
        self.delta2_share = delta2_share
        
        self.omegameas1_share = omegameas1_share
        
        self.omegameas2_share = omegameas2_share
        
        ## A shares.Share object representing the indicator for which encoder function to run
        self.enc_flag = enc_flag
        
        ## The number of runs of the state machine     
        self.runs = 0
        
        ## The period (in us) of the task
        self.period = period
        
        ## The time to run the next iteration of the task
        self.next_time = time.ticks_add(time.ticks_us(), self.period) 
        
        self.time_next = 0
        
        ## A encoder.Encoder object used to run encoder function in the task
        self.encode = encoder.Encoder()
        
        
    def run(self):
        ''' @brief      Runs one iteration of the FSM.
            @details    Continuously updates encoder position while running 
                        other encoder function when prompted to through the enc_flag.
        '''
        if(time.ticks_us() >= self.next_time):
                if self.enc_flag.read() == 1:
                    self.pos1_share.write(self.encode.set_position(0,1))
                    
                elif self.enc_flag.read() == 2:
                    self.delta1_share.write(self.encode.get_delta(1))
                    print('Encoder 1 Delta is {:}'.format(self.delta1_share.read()))
                    
                elif self.enc_flag.read() == 3:
                    self.pos2_share.write(self.encode.set_position(0,2))
                        
                elif self.enc_flag.read() == 4:
                    self.delta2_share.write(self.encode.get_delta(2))
                    print('Encoder 2 Delta is {:}'.format(self.delta2_share.read()))
                    
                elif self.enc_flag.read() == 5:    
                    self.pos1_share.write(self.encode.get_position(1))
                    print('Encoder 1 Position is {:}'.format(self.pos1_share.read()))
                    
                elif self.enc_flag.read() == 6:    
                    self.pos2_share.write(self.encode.get_position(2))
                    print('Encoder 2 Position is {:}'.format(self.pos2_share.read()))
                    
    
                else:
                    self.encode.update()
                    self.time_diff = time.ticks_diff(time.ticks_us(),self.time_next)
                    self.time_next = time.ticks_us()
                    
                    self.pos1_share.write(self.encode.get_position(1))
                    # print(self.encode.get_position(1))
                    self.delta1_share.write(self.encode.get_delta(1))
                

                    self.pos2_share.write(self.encode.get_position(2))
                    # print(self.encode.get_position(2))
                    self.delta2_share.write(self.encode.get_delta(2))
                    
                    self.omegameas1_share.write(self.delta1_share.read()*2*math.pi/(1020*self.time_diff/1000000))
                    self.omegameas2_share.write(self.delta2_share.read()*2*math.pi/(1020*self.time_diff/1000000))
                    
                    self.next_time += self.period
                    self.runs += 1
