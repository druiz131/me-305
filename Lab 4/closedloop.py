# -*- coding: utf-8 -*-
"""
Created on Fri Nov  5 10:49:52 2021

@author: melab15
"""

class Closed_Loop:
    ''' @brief                 
         @details                
    '''
    def init(self, p_gain, max_lim, min_lim):
        ''' @brief            
            @details          
        '''
        ## Proportional gain value
        self.p_gain = p_gain
        ## Upper bound saturation limit
        self.max_lim = max_lim
        ## Lower bound saturation limit
        self.min_lim = min_lim
        # self.sat_limits = sat_limits
        ## Reference speed for encoder
        self.omega_reference = 0
        ## Measured speed for encoder
        self.omega_measured = 0
        
        
        
    def update(self,omega_reference,omega_measured):
        ''' @brief
            @details
        '''
        
        self.max_lim = 100
        self.min_lim = -100
        self.omega_reference = omega_reference
        self.omega_measured = omega_measured
        self.actuation = self.p_gain*(self.omega_reference-self.omega_measured)
        
        if self.actuation >= self.max_lim:
            self.actuation = self.max_lim
        elif self.actuation <= self.min_lim:
            self.actuation = self.min_lim
           
       # if self.actuation >= self.sat_limits[2]:
       #     self.actuation = self.sat_limits[2]
       # elif self.actuation <= self.sat_limits[1]:
       #     self.actuation = self.sat_limits[1]  
        
        return self.actuation
   
    def get_Kp(self):
        ''' @brief
            @ details
        '''
        
        return self.p_gain
    
    def set_Kp(self,Kp):
        ''' @brief
            @details
        '''
        
        self.p_gain = Kp
        
    
        