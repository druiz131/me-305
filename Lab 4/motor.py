"""
    @file           motor.py
    @brief          Driver class for encoder project.
    @details        Defines functions used in the task_motor. 
"""


import pyb
import utime

class DRV8847:
    ''' @brief          A motor driver class for the DRV8847 from TI
        @details        Objects of this class can be used to configure the
                        DRV8847 motor driver and to create one or more objects
                        of the Motor class which can be used to perform motor control
    '''
    def __init__(self):
        ''' @brief      Initializes and returns a DRV8847 object
        '''
        ## Initializes the nSLEEP pin, A 15
        self.pinSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
        ## Initializes the fault pin, B2
        self.pinB2 = pyb.Pin(pyb.Pin.cpu.B2)
        ## Initializes the fault callback
        self.fault = pyb.ExtInt(self.pinB2, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=self.fault_cb)
        pass
    
    def enable(self):
        ''' @brief      Brings the DRV8847 out of sleep mode
        '''
        ## Disables the fault callback
        self.fault.disable()
        ## Sets the nSLEEp pin to high
        self.pinSLEEP.high()
        ## Sets a delay of 25 us
        utime.sleep_us(25)
        ## Enables the fault callback
        self.fault.enable()
        pass
    
    def disable(self):
        ''' @brief      Puts the DRV8847 in sleep mode
        '''
        ## Sets the nSLEEP pin to low
        self.pinSLEEP.low()
        pass
    
    def fault_cb(self, IRQ_src):
        ''' @brief      Callback function to run on fault condition
            @param      IRQ_src The source of the interrupt request
        '''
        
        ## Sets the nSLEEP pin to low
        self.pinSLEEP.low()
        print('Fault detected, program stopped')
        pass
    
    def motor(self, motor):
        ''' @brief      A motor class for one channel of the DRV8847
            @details    Objects of this class can be used to apply PWM 
                        to a given DC motor
            @return     The initialized motor class
        '''
        
        return Motor(motor)
    
class Motor:
    ''' @brief      A motor class for one channel of the DRV8847
        @details    Objects of this class can be used to apply PWM to a
                    given DC motor
    '''
    
    def __init__(self, motor):
        ''' @brief      Initializes and returns a motor object associatted with the DRV8847
            @details   
        '''
        ## Initializes timer 3
        self.tim3 = pyb.Timer(3, freq = 20000)
        ## Initializes pin B4
        self.pinIN1 = pyb.Pin(pyb.Pin.cpu.B4)
        ## Initializes pin B5
        self.pinIN2 = pyb.Pin(pyb.Pin.cpu.B5)
        ## Initializes pin B0
        self.pinIN3 = pyb.Pin(pyb.Pin.cpu.B0)
        ## Initializes pin B1
        self.pinIN4 = pyb.Pin(pyb.Pin.cpu.B1)
        ## Configures channel 1 for timer 3
        self.t3ch1 = self.tim3.channel(1, pyb.Timer.PWM, pin=self.pinIN1)
        ## Configures channel 2 for timer 3
        self.t3ch2 = self.tim3.channel(2, pyb.Timer.PWM, pin=self.pinIN2)
        ## Configures channel 3 for timer 3
        self.t3ch3 = self.tim3.channel(3, pyb.Timer.PWM, pin=self.pinIN3)
        ## Configures channel 4 for timer 3
        self.t3ch4 = self.tim3.channel(4, pyb.Timer.PWM, pin=self.pinIN4)
        ## Input parameter that is used to initialize the set_duty function
        self.motor = motor
        pass
    
    def set_duty(self, duty):
        ''' @brief      Sets the PWm duty cylce for the motor channel
            @details    This method sets the duty cycle to be sent to the 
                        motor to the given level. posotive values cause effort
                        in one dierction, negative values in the opposite
                        direction
            @param      duty    A signed number holding the duty cycle
                                of the PWM signal sent to the motor
        '''
        
        if self.motor == 1:
            if duty > 0:
                self.t3ch1.pulse_width_percent(abs(duty))
                self.t3ch2.pulse_width_percent(0)
            elif duty < 0:
                self.t3ch1.pulse_width_percent(0)
                self.t3ch2.pulse_width_percent(abs(duty))
            pass
        if self.motor == 2:
            if duty > 0:
                self.t3ch3.pulse_width_percent(abs(duty))
                self.t3ch4.pulse_width_percent(0)
            elif duty < 0:
                self.t3ch3.pulse_width_percent(0)
                self.t3ch4.pulse_width_percent(abs(duty))
            pass
    

# if __name__ == '__main__':
    
#     motor_drv = DRV8847()
#     motor_1 = motor_drv.motor(1)
#     motor_2 = motor_drv.motor(2)
    
#     motor_drv.disable()
#     motor_drv.enable()
#     motor_1.set_duty(100)
#     motor_2.set_duty(100)

        
    
    
    
    
    
    
    
    

