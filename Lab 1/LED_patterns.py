# All functions should be defined bwefore the if __name__... block
'''
    @file       LED_patterns.py
    @brief      Changes LED patterns based on button input
    @details    This script programs a nucleo to cycle between three different 
                wave patterns(square, sin, and sawtooth) projected on a LED
                based on a button input. This is a video showing how the LED Light 
                works: https://cpslo-my.sharepoint.com/:v:/g/personal/dyruiz_calpoly_edu/EelOXbklHTRPhYRStjdc8MwBYotzM8h6hAvvF1_N4mXBbA?e=LdF9l4.
                This is a link that shows the state-transition diagram of the system:
                https://cpslo-my.sharepoint.com/:b:/g/personal/dyruiz_calpoly_edu/EZoqUYLOOw9JtdwY-EfLCNQBVMz44ghOhmjZzWcRE0QXhA?e=CDGB3e    
    @author     Cesar Santana
    @author     Dylan Ruiz
    @date       October 8, 2021
    
'''
import pyb
import utime
import math

def ButtonPushed(IRQ_src):
    '''
    @brief      Indicates when the button is pushed
    @details    This function indicates when the button is pushed by changing
                the state variable to 1
    @param      IRQ_src Indicates that the button was pushed
    @return     A value of 1 indicating the button was pushed
    '''
    global button
    button = 1

def set_timer():
    '''
    @brief      Sets the start time for the timer
    @details    This function sets a new reference point everytime the button 
                is pressed
    @param
    @return     The new reference point
    '''
    global starttime
    starttime = utime.ticks_ms()
    return starttime

def update_timer():
    '''
    @brief      Calculates the elapsed time of the wave pattern
    @details    This function calculates the elapsed time by first recording
                a stop time and then subtracting that from the reference point
    @param
    @return     The time elapsed
    '''
    stoptime = utime.ticks_ms()
    duration = utime.ticks_diff(stoptime, starttime)/1000
    return duration

def update_stw(current_time):
    '''
    @brief      Calculates the brightness for a sawtooth wave
    @details    This function calculates the brighness for a sawtooth wave
                at a given input of time
    @param      current_time The elapsed time of the wave pattern
    @return     Brightness of the LED at the time
    '''
    return 100*(current_time%1)

def update_sqw(current_time):
    '''
    @brief      Calculates the brightness for a square wave
    @details    This function calculates the brightness for a square wave 
                at a given input of time
    @param      current_time The elapsed time of the wave pattern
    @return     Brightness of the LED at the time
    '''
    return 100*(current_time%1<0.5)

def update_sw(current_time):
    '''
    @brief      Calculates the brighness for a sin wave
    @details    This function calculates the brightness for a sin wave
                at a given input of time
    @param      current_time The elapsed time of the wave pattern
    @return     Brightness of the LED at the time
    '''
    return 100*(0.5 + 0.5*math.sin(math.pi* current_time/5))

if __name__ == '__main__':
    ## Creates a pin object for PC13
    pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
    
    ## Associated callback function with the pin by setting up an external interrupt
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=ButtonPushed)
    
    ## Creates a pin object for PA5
    pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
    
    ## Creates a timer object using timer 2 - trigger at 20kHz
    tim2 = pyb.Timer(2, freq = 20000)
    
    ## Timer channel 1 is configured PWM mode with PA5 linked to the timer channel
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
    
    ## State variable indicating whether button is pressed or not pressed
    button = 0
    
    ## Variable indicating what state/pattern the program is running
    state = 0
    
    ## Indicates how many full cycles the program has run
    runs = 0
    
    while(True):
        try:
            # This is the initilization state that prints instruction on how 
            # to begin the program
            if (state == 0):
                print('Hello, press the blue button to cycle through LED patterns')
                state = 1             
            
            # This is the idler state where the program waits for button input
            # from the user to initialize pattern cycle
            elif (state == 1):
                
                # Once the button is pressed, the timer will be reset and the 
                # program will transition to the square wave pattern and the
                # button will revert to a value of 0
                if (button == 1):              
                    start = set_timer()
                    print('Square Wave Pattern')
                    state = 2
                    button = 0
                    
            # This is the square wave pattern, the timer will continually 
            # update and calculate a new brightness level until the button is
            # pressed
            elif (state == 2):
                duration = update_timer()
                brightness = update_sqw(duration)
                t2ch1.pulse_width_percent(brightness)
                
                # Once the button is pressed, the timer will be reset and the 
                # program will transition to the sin wave pattern and the
                # button will revert to a value of 0
                if (button == 1):                
                    start = set_timer()
                    print('Sin Wave Pattern')
                    state = 3
                    button = 0              
             
            # This is the sin wave pattern, the timer will continually 
            # update and calculate a new brightness level until the button is
            # pressed    
            elif (state == 3):
                duration = update_timer()
                brightness = update_sw(duration)
                t2ch1.pulse_width_percent(brightness)
                
                # Once the button is pressed, the timer will be reset and the 
                # program will transition to the sawtooth wave pattern and the
                # button will revert to a value of 0
                if (button == 1):               
                    start = set_timer()
                    print('Sawtooth Wave Pattern')
                    state = 4
                    button = 0
            
            # This is the sawtooth wave pattern, the timer will continually 
            # update and calculate a new brightness level until the button is
            # pressed    
            elif (state == 4):
                duration = update_timer()
                brightness = update_stw(duration)
                t2ch1.pulse_width_percent(brightness)
                
                # Once the button is pressed, the timer will be reset and the 
                # program will transition to the square wave pattern and the
                # button will revert to a value of 0
                if (button == 1):               
                    start = set_timer()
                    print('Square Wave Pattern')
                    state = 2
                    button = 0
                        
            runs += 1
        
        # This function allows for the program to be terminated, with a
        # keyboard input of ctrl+c, a message indicating the program has
        # been terminatted will then be printed
        except KeyboardInterrupt:
              break
    print('Program Terminating')

