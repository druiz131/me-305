# All functions should be defined bwefore the if __name__... block
import pyb
import time
 

def ButtonPushed(IRQ_src):
    global button
    button = 1



if __name__ == '__main__':
    ## the next state to run as the FSM iterates
    pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
    pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
    tim2 = pyb.Timer(2, freq = 20000)
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=ButtonPushed)
    button = 0
    state = 0
    runs = 0
    while(True):
        try:
            if (state == 0):
                #run state code 0
                print('Hello, press the blue button to cycle through LED patterns')
                state = 1             
                
            elif (state == 1):
                #run state 1 code
                if (button == 1):
                    state = 2
                    button = 0
                    
                
            elif (state == 2):
                print('state 2')
                t2ch1.pulse_width_percent(100)
                if button == 1:
                    state = 3
                    button = 0              
                    pass
                
            elif (state == 3):
                print('state 3')
                t2ch1.pulse_width_percent(50)
                if button == 1:
                    state = 4
                    button = 0
                    pass 
                
            elif (state == 4):
                print('state 4')
                t2ch1.pulse_width_percent(0)
                if button == 1:
                    state = 2
                    button = 0
                    pass
                        
            runs += 1
            time.sleep(1)
            
        except KeyboardInterrupt:
              break
    print('Program Terminating')
