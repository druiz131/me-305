from pyb import I2C

class BNO055:
    
    def __init__(self):
        self.i2c = I2C(1,I2C.MASTER)
        self.buf = bytearray(6)
        self.coeff_list = []
        
    def op_mod(self, register):
        self.i2c.mem_write(register, 0x28, 0x3D, timeout=5000)
    
    def cal_status(self):
        self.cal_bytes = self.i2c.mem_read(1, 0x28, 35)
        
        self.cal_status = (self.cal_bytes[0] & 0b11,
                           (self.cal_bytes[0] & 0b11 << 2) >> 2,
                           (self.cal_bytes[0] & 0b11 << 4) >> 4,
                           (self.cal_bytes[0] & 0b11 << 6) >>6)
        print('Values:', self.cal_status)
    
    def cal_coeff(self):
        self.cal_coeff = self.i2c.mem_read(22, 0x28, 55)
        self.coeff_list = []
        for self.a in range(0,21):
            self.bin_value = bin(self.cal_coeff[self.a])
            self.coeff_list.append(self.bin_value)
        
        
    def cal_write(self, data):
        self.cal_values = data
        self.i2c.mem_write(self.cal_values, 0x28,55)
      
        
        
    def Eul(self):
        self.bytes_eul = self.i2c.mem_read(self.buf, 0x28, 0x1A)
        self.heading = self.bytes_eul[0] | self.bytes_eul[1] << 8 
        self.roll = self.bytes_eul[2] | self.bytes_eul[3] << 8
        self.pitch = self.bytes_eul[4] | self.bytes_eul[5] << 8
        
        if self.heading > 32767:
            self.heading -= 65536
        if self.roll > 32767:
            self.roll -= 65536
        if self.pitch > 32767:
            self.pitch -= 65536
    
    def ang_vel(self):
        self.bytes_gyr = self.i2c.mem_read(self.buf, 0x28, 14)
        self.gyr_x = self.bytes_gyr[0] | self.bytes_gyr[1] << 8 
        self.gyr_y = self.bytes_gyr[2] | self.bytes_gyr[3] << 8
        self.gyr_z = self.bytes_gyr[4] | self.bytes_gyr[5] << 8
        
        if self.gyr_x > 32767:
            self.gyr_x -= 65536
        if self.gyr_y > 32767:
            self.gyr_y -= 65536
        if self.gyr_z > 32767:
            self.gyr_z -= 65536
